const {
  FuseBox,
  WebIndexPlugin,
  BabelPlugin,
  QuantumPlugin,
  CSSPlugin,
} = require('fuse-box');

const isProduction = process.env.NODE_ENV === 'production';

const fuse = FuseBox.init({
  homeDir: 'src',
  target: 'browser@es6',
  output: 'dist/static/admin/$name.js',
  sourceMaps: true,
  plugins: [
    !isProduction &&
      WebIndexPlugin({
        template: 'src/assets/index.html',
        title: 'air cms admin',
      }),
    BabelPlugin({
      config: {
        sourceMaps: true,
        presets: ['env', 'react', 'stage-0'],
        plugins: ['transform-runtime'],
      },
    }),
    isProduction && QuantumPlugin(),
    CSSPlugin(),
  ],
  alias: {
    app: '~/app',
  },
});

// run dev server
if (!isProduction) {
  fuse.dev({
    fallback: 'index.html',
    proxy: {
      '/api/*': {
        target: 'http://localhost:5000',
        changeOrigin: true,
      },
    },
    port: 4444,
  });
}

const bundle = fuse.bundle('app').instructions(' > app/index.jsx');

if (!isProduction) {
  bundle.hmr().watch('src/**');
}

fuse.run();
