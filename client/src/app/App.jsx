import React from 'react';
import { Route } from 'react-router-dom';
import {
  Main,
  Users,
  User,
  Signin,
  Info,
  Records,
  Record,
  CreateRecord,
} from 'app/pages';
import { Header, Footer } from 'app/components/elements';
import { AccountLoader } from 'app/modules/account';

import './App.css';

const App = () => (
  <main className="main">
    <div className="main__content">
      <AccountLoader>
        <Header />
        <Route path="/admin" exact component={Main} />
        <Route path="/admin/users" exact component={Users} />
        <Route path="/admin/users/:id" component={User} />
        <Route path="/admin/signin" component={Signin} />
        <Route path="/admin/info" component={Info} />
        <Route path="/admin/blog/records" exact component={Records} />
        <Route path="/admin/blog/create-record" component={CreateRecord} />
        <Route path="/admin/blog/records/:id" component={Record} />
      </AccountLoader>
    </div>
    <Footer />
  </main>
);

export default App;
