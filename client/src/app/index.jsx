import React from 'react';
import { render } from 'react-dom';
import { ConnectedRouter } from 'react-router-redux';
import { Provider } from 'react-redux';
import { store, history } from 'app/core/store';
import { bootstrap } from 'app/core/bootstrap';
import App from './App';
import 'bulma/css/bulma.css';

bootstrap(store);

const renderApp = () =>
  render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </Provider>,
    document.querySelector('#app')
  );

if (module.hot) {
  module.hot.accept(renderApp);
}

renderApp();
