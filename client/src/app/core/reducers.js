import { combineReducers } from 'redux';
import usersReducer from 'app/modules/users/reducers';
import userReducer from 'app/modules/user/reducers';
import rolesReducer from 'app/modules/roles/reducers';
import accountReducer from 'app/modules/account/reducers';

import { routerReducer } from 'react-router-redux';

export default combineReducers({
  users: usersReducer,
  user: userReducer,
  roles: rolesReducer,
  account: accountReducer,
  router: routerReducer,
});
