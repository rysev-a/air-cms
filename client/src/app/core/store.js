import { createStore, applyMiddleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import { createLogger } from 'redux-logger';
import createHistory from 'history/createBrowserHistory';
import thunk from 'redux-thunk';
import reducers from 'app/core/reducers';
import { REDUX_LOGGER } from 'app/settings';

export const configureStore = (
  history,
  initialState = {},
  customMiddlewares = []
) => {
  const middlewares = [thunk, routerMiddleware(history), ...customMiddlewares];

  if (process.env.NODE_ENV == 'development' && REDUX_LOGGER) {
    middlewares.push(createLogger());
  }

  const store = createStore(
    reducers,
    initialState,
    applyMiddleware(...middlewares)
  );

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./reducers', () => {
      const nextRootReducer = require('app/core/reducers');
      store.replaceReducer(nextRootReducer);
    });
  }

  // debbug helpers
  if (process.env.NODE_ENV === 'development') {
    window.store = store;
    window.getState = state => store.getState()[state];
  }

  return store;
};

export const history = createHistory();
export const store = configureStore(history);
