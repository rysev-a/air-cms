import rolesActions from 'app/modules/roles/actions';
import accountActions from 'app/modules/account/actions';

export const bootstrap = store => {
  store.dispatch(rolesActions.load());
  store.dispatch(accountActions.load());
};
