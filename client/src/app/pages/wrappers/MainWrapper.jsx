import React from 'react';
import PropTypes from 'prop-types';
import { AsideMenu } from 'app/components/elements';

const MainWrapper = ({ children }) => (
  <section className="section">
    <div className="container">
      <div className="columns">
        <div className="column is-one-third">
          <AsideMenu />
        </div>
        <div className="column">{children}</div>
      </div>
    </div>
  </section>
);

MainWrapper.propTypes = {
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

export default MainWrapper;
