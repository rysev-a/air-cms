import React from 'react';
import Users from 'app/modules/users';
import { MainWrapper } from './wrappers';

const UsersPage = () => (
  <MainWrapper>
    <Users />
  </MainWrapper>
);

export default UsersPage;
