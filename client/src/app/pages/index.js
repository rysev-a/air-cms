import Main from './Main';
import Users from './Users';
import User from './User';
import Signin from './Signin';
import Info from './Info';

// blog pages
import Records from './blog/Records';
import Record from './blog/Record';
import CreateRecord from './blog/CreateRecord';

export { Main, Users, User, Signin, Info, Records, Record, CreateRecord };
