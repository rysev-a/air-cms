import React from 'react';
import { Signin } from 'app/modules/sign';

const SigninPage = () => (
  <div className="container">
    <Signin />
  </div>
);

export default SigninPage;
