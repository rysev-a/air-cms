import React from 'react';
import { MainWrapper } from './wrappers';

const MainPage = () => (
  <MainWrapper>
    <div className="main-page">
      <h1 className="is-size-1">Main Page</h1>
    </div>
  </MainWrapper>
);

export default MainPage;
