import React from 'react';
import User from 'app/modules/user';
import { MainWrapper } from './wrappers';

const UserPage = () => (
  <MainWrapper>
    <User />
  </MainWrapper>
);

export default UserPage;
