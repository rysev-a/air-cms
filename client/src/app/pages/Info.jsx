import React from 'react';
import { Info } from 'app/components/elements';

const InfoPage = () => (
  <div className="container">
    <Info />
  </div>
);

export default InfoPage;
