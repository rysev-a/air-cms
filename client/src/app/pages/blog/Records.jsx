import React from 'react';
import Records from 'app/modules/blog/records';
import { MainWrapper } from 'app/pages/wrappers';

const RecordsPage = () => (
  <MainWrapper>
    <Records />
  </MainWrapper>
);

export default RecordsPage;
