import React from 'react';
import { Record } from 'app/modules/blog/record';
import { MainWrapper } from 'app/pages/wrappers';

const RecordPage = () => (
  <MainWrapper>
    <Record />
  </MainWrapper>
);

export default RecordPage;
