import React from 'react';
import { CreateRecord } from 'app/modules/blog/record';
import { MainWrapper } from 'app/pages/wrappers';

const CreateRecordPage = () => (
  <MainWrapper>
    <CreateRecord />
  </MainWrapper>
);

export default CreateRecordPage;
