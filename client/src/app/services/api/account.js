import axios from 'app/core/axios';
import { API_URL } from 'app/settings';

const accountApi = {
  signin: data => axios.post(`${API_URL}/account/signin`, data),
  signup: data => axios.post(`${API_URL}/account/signup`, data),
  signout: () => axios.post(`${API_URL}/account/signout`),
  load: () => axios.get(`${API_URL}/account`),
};

export default accountApi;
