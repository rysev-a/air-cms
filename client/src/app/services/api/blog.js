import axios from 'axios';
import { API_URL } from 'app/settings';

const records = {
  get: ({ pagination }) =>
    axios.get(`${API_URL}/blog/records?page=${pagination.page}`),
  delete: id => axios.delete(`${API_URL}/blog/records/${id}`),
  post: values => axios.post(`${API_URL}/blog/records`, values),
};

const record = {
  get: id => axios.get(`${API_URL}/blog/records/${id}`),
  put: ({ id, values }) => axios.put(`${API_URL}/blog/records/${id}`, values),
};

const blogApi = { records, record };

export default blogApi;
