import axios from 'app/core/axios';
import { API_URL } from 'app/settings';

const userApi = {
  get: id => axios.get(`${API_URL}/users/${id}`),
  put: ({ id, values }) => axios.put(`${API_URL}/users/${id}`, values),
};

export default userApi;
