import axios from 'app/core/axios';
import { API_URL } from 'app/settings';

const usersApi = {
  get: ({ pagination, filter }) =>
    axios.get(
      `${API_URL}/users?page=${pagination.page}&filter=${JSON.stringify(
        filter
      )}`
    ),
  post: data => axios.post(`${API_URL}/users`, data),
  delete: id => axios.delete(`${API_URL}/users/${id}`),
};

export default usersApi;
