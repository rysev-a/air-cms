import axios from 'app/core/axios';
import { API_URL } from 'app/settings';

const rolesApi = {
  get: () => axios.get(`${API_URL}/roles`),
};

export default rolesApi;
