import constants from '../constants';

const defaultState = () => [];

const usersFilterReducer = (state = defaultState(), action) => {
  switch (action.type) {
    case constants.USER_LIST_UPDATE_FILTER: {
      const { key, value } = action.payload;
      const updatedState = state.filter(item => item.key != key);

      if (!value) {
        return updatedState;
      }

      // if value, add filter
      return [...updatedState, { key, value }];
    }

    case constants.USER_LIST_RESET:
      return defaultState();

    default:
      return state;
  }
};

export default usersFilterReducer;
