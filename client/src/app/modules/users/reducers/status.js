import constants from '../constants';

const defaultState = () => ({
  loaded: false,
  processing: false,
});

const usersStatusReducer = (state = defaultState(), action) => {
  switch (action.type) {
    case constants.USER_LIST_FETCH_START:
    case constants.USER_LIST_REMOVE_START:
      return {
        ...state,
        processing: true,
      };

    case constants.USER_LIST_FETCH_ERROR:
    case constants.USER_LIST_REMOVE_ERROR:
      return {
        ...state,
        processing: false,
      };

    case constants.USER_LIST_FETCH_SUCCESS:
      return {
        processing: false,
        loaded: true,
      };

    case constants.USER_LIST_RESET:
      return defaultState();

    default:
      return state;
  }
};

export default usersStatusReducer;
