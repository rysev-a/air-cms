import { combineReducers } from 'redux';
import usersDataReducer from './data';
import usersPaginationReducer from './pagination';
import usersStatusReducer from './status';
import usersFilterReducer from './filter';

const usersReducer = combineReducers({
  data: usersDataReducer,
  pagination: usersPaginationReducer,
  status: usersStatusReducer,
  filter: usersFilterReducer,
});

export default usersReducer;
