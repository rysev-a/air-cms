import constants from '../constants';

const defaultState = () => [];

const usersDataReducer = (state = defaultState(), action) => {
  switch (action.type) {
    case constants.USER_LIST_FETCH_SUCCESS:
      return action.payload.data;

    case constants.USER_LIST_RESET:
      return defaultState();

    default:
      return state;
  }
};

export default usersDataReducer;
