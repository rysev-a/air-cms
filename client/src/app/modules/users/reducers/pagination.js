import constants from '../constants';

const defaultState = () => ({
  page: 0,
  pageCount: 0,
});

const usersPaginationReducer = (state = defaultState(), action) => {
  switch (action.type) {
    case constants.USER_LIST_FETCH_SUCCESS:
      return {
        ...state,
        page: action.payload.page,
        pageCount: action.payload.page_count,
      };

    case constants.USER_LIST_RESET:
      return defaultState();

    default:
      return state;
  }
};

export default usersPaginationReducer;
