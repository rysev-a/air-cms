export default {
  USER_LIST_FETCH_START: 'user list fetch start',
  USER_LIST_FETCH_SUCCESS: 'user list fetch success',
  USER_LIST_FETCH_ERROR: 'user list fetch error',

  USER_LIST_REMOVE_START: 'user list remove start',
  USER_LIST_REMOVE_ERROR: 'user list remove error',

  USER_LIST_RESET: 'user list resest',
  USER_LIST_UPDATE_FILTER: 'user list update filter',
};
