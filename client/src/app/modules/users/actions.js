import { shallowEqual } from 'recompose';
import { usersApi } from 'app/services/api';
import constants from './constants';

const usersActions = {
  update: (
    { users: { filter, pagination } },
    { users: { filter: prevFilter } }
  ) => dispatch => {
    if (!shallowEqual(filter, prevFilter)) {
      dispatch(usersActions.load({ users: { filter, pagination } }));
    }
  },

  load: ({ users }) => dispatch => {
    dispatch(usersActions.fetchStart());
    return usersApi
      .get({ ...users })
      .then(({ data }) => dispatch(usersActions.fetchSuccess(data)))
      .catch(() => dispatch(usersActions.fetchError()));
  },

  remove: id => dispatch => {
    dispatch(usersActions.removeStart());
    return usersApi.delete(id).catch(error => {
      dispatch(usersActions.removeError());
    });
  },

  removeStart: () => ({
    type: constants.USER_LIST_REMOVE_START,
  }),

  removeError: () => ({
    type: constants.USER_LIST_REMOVE_ERROR,
  }),

  fetchSuccess: data => ({
    type: constants.USER_LIST_FETCH_SUCCESS,
    payload: data,
  }),

  fetchError: () => ({
    type: constants.USER_LIST_FETCH_ERROR,
  }),

  fetchStart: () => ({
    type: constants.USER_LIST_FETCH_START,
  }),

  updateFilter: (key, value) => ({
    type: constants.USER_LIST_UPDATE_FILTER,
    payload: { key, value },
  }),

  reset: () => ({
    type: constants.USER_LIST_RESET,
  }),
};

export default usersActions;
