import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { Icon, Pagination, Processing } from 'app/components/ui';
import { Input } from 'app/components/form';
import './Users.css';

const Users = ({
  users: { data, pagination, status, filter },
  pageSet,
  remove,
  updateFilter,
}) => (
  <div className="users">
    <Processing processing={status.processing} />
    <h1 className="is-size-1">Users</h1>
    <table className="table">
      <thead>
        <tr>
          <th className="is-unselectable">
            <Input
              onChange={e => updateFilter('email', e.target.value)}
              modificators={['is-inline']}
              placeholder="Email"
            />
          </th>
          <th className="is-unselectable">First Name</th>
          <th className="is-unselectable">Last Name</th>
          <th className="is-unselectable">Patronymic</th>
          <th className="is-unselectable">Phone</th>
          <th className="is-unselectable">Role</th>
          <th className="is-unselectable">actions</th>
        </tr>
      </thead>
      <tbody>
        {data.map(user => (
          <tr key={user.id}>
            <td>{user.email}</td>
            <td>{user.first_name}</td>
            <td>{user.last_name}</td>
            <td>{user.patronymic}</td>
            <td>{user.phone}</td>
            <td>{user.role.name}</td>
            <td>
              <NavLink to={`/users/${user.id}`}>
                <Icon type="pencil-alt" color="dark" />
              </NavLink>

              <a onClick={remove.bind(null, user.id)}>
                <Icon type="trash" color="danger" />
              </a>
            </td>
          </tr>
        ))}
      </tbody>
      <tfoot>
        <tr>
          <td colSpan="6">
            <Pagination pagination={pagination} pageSet={pageSet} />
          </td>
        </tr>
      </tfoot>
    </table>
  </div>
);

Users.propTypes = {
  users: PropTypes.object,
  pageSet: PropTypes.func,
  remove: PropTypes.func,
  updateFilter: PropTypes.func,
};

export default Users;
