import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Users from '../components/Users';
import usersActions from '../actions';
import { compose, lifecycle, withHandlers } from 'recompose';

const mapStateToProps = ({ users }) => ({ users });

const mapDispatchToProps = dispatch =>
  bindActionCreators(usersActions, dispatch);

const UsersContainer = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  lifecycle({
    componentDidMount() {
      this.props.load(this.props);
    },
    componentDidUpdate(prevProps) {
      this.props.update(this.props, prevProps);
    },
    componentWillUnmount() {
      this.props.reset();
    },
  }),
  withHandlers({
    pageSet: ({ load, users }) => page => {
      load({
        users: {
          ...users,
          pagination: {
            ...users.pagination,
            page: page,
          },
        },
      });
    },
    remove: props => id => {
      props.remove(id).then(() => {
        props.load(props);
      });
    },
  })
)(Users);

export default UsersContainer;
