import moxios from 'moxios';
import roleActions from '../actions';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { defaultState } from '../reducers';
import constants from '../constants';
import { API_URL } from 'app/settings';

const mockStore = configureMockStore([thunk]);

describe('test roles', () => {
  beforeEach(() => moxios.install());
  afterEach(() => moxios.uninstall());

  const rolesPayload = [{ id: 1, name: 'admin', id: 2, name: 'manager' }];

  it('fetch foles', () => {
    const store = mockStore({ roles: defaultState() });

    moxios.stubRequest(`${API_URL}/roles`, {
      status: 200,
      response: rolesPayload,
    });

    const expectedActions = [
      { type: constants.ROLE_LIST_FETCH_START },
      { type: constants.ROLE_LIST_FETCH_SUCCESS, payload: rolesPayload },
    ];

    return store
      .dispatch(roleActions.load())
      .then(({ data }) => expect(store.getActions()).toEqual(expectedActions));
  });
});
