import constants from './constants';

export const defaultState = () => ({
  data: [],
  loaded: false,
  processing: false,
});

const rolesReducer = (state = defaultState(), action) => {
  switch (action.type) {
    case constants.ROLE_LIST_FETCH_SUCCESS:
      return {
        ...state,
        data: action.payload,
        processing: false,
        loaded: true,
      };

    case constants.ROLE_LIST_FETCH_START:
      return {
        ...state,
        processing: true,
      };

    case constants.ROLE_LIST_RESET:
      return defaultState();

    default:
      return state;
  }
};

export default rolesReducer;
