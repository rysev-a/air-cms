export default {
  ROLE_LIST_FETCH_START: 'role list fetch start',
  ROLE_LIST_FETCH_SUCCESS: 'role list fetch success',
  ROLE_LIST_FETCH_ERROR: 'role list fetch error',
  ROLE_LIST_RESET: 'role list reset',
};
