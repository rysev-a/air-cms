import constants from './constants';
import { rolesApi } from 'app/services/api';

const rolesActions = {
  load: () => dispatch => {
    dispatch(rolesActions.fetchStart());
    return rolesApi
      .get()
      .then(({ data }) => dispatch(rolesActions.fetchSuccess(data)));
  },

  fetchStart: () => ({
    type: constants.ROLE_LIST_FETCH_START,
  }),

  fetchSuccess: data => ({
    type: constants.ROLE_LIST_FETCH_SUCCESS,
    payload: data,
  }),

  reset: () => ({
    type: constants.ROLE_LIST_RESET,
  }),

  submit: values => dispatch => {},
};

export default rolesActions;
