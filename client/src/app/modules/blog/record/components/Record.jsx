import React from 'react';
import PropTypes from 'prop-types';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Button } from 'app/components/ui';

const Record = ({
  values,
  handleChange,
  handleBlur,
  handleSubmit,
  setFieldValue,
}) => (
  <div className="record">
    <form className="record-form" onSubmit={handleSubmit}>
      <div className="field">
        <label className="label">Title:</label>
        <div className="control">
          <input
            name="title"
            className="input"
            value={values.title}
            onChange={handleChange}
            onBlur={handleBlur}
          />
        </div>
      </div>

      <div className="field">
        <label className="label">Anons:</label>
        <div className="control">
          <input
            name="anons"
            className="input"
            value={values.anons}
            onChange={handleChange}
            onBlur={handleBlur}
          />
        </div>
      </div>

      <div className="field">
        <label className="label">Description:</label>
        <div className="control">
          <input
            name="description"
            className="input"
            value={values.description}
            onChange={handleChange}
            onBlur={handleBlur}
          />
        </div>
      </div>

      <div className="field">
        <label className="label">Content:</label>
        <div className="control">
          <CKEditor
            editor={ClassicEditor}
            config={{
              toolbar: [
                'heading',
                '|',
                'bold',
                'italic',
                'link',
                'bulletedList',
                'numberedList',
                'blockQuote',
              ],
            }}
            data={values.content}
            onChange={(event, editor) =>
              setFieldValue('content', editor.getData())
            }
          />
        </div>
      </div>

      <Button type="submit">Save</Button>
    </form>
  </div>
);

Record.propTypes = {
  values: PropTypes.object,
  handleBlur: PropTypes.func,
  handleChange: PropTypes.func,
  handleSubmit: PropTypes.func,
  setFieldValue: PropTypes.func,
};

export default Record;
