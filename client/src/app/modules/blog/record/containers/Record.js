import { withRouter } from 'react-router';
import { withFormik } from 'formik';
import { compose, lifecycle } from 'recompose';
import { connect } from 'react-redux';
import { blogApi } from 'app/services/api';
import { withItem } from 'app/hoc/item';
import Record from '../components/Record';

export default compose(
  withRouter,
  connect(),
  withItem(blogApi.record),
  lifecycle({
    componentDidMount() {
      this.props.load(this.props);
    },
  }),
  withFormik({
    mapPropsToValues: ({ item }) => ({
      title: item.data.title || '',
      anons: item.data.anons || '',
      description: item.data.description || '',
      content: item.data.content || '',
    }),
    handleSubmit: (values, { props, setSubmitting, setErrors }) => {
      props.update({ values, setSubmitting, setErrors });
    },
    enableReinitialize: true,
  })
)(Record);
