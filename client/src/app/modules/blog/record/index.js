import Record from './containers/Record';
import CreateRecord from './containers/CreateRecord';

export { Record, CreateRecord };
