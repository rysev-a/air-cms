import { blogApi } from 'app/services/api';
import { withList } from 'app/hoc/list';
import Records from '../components/Records';

export default withList(blogApi.records)(Records);
