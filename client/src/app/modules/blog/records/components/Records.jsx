import React from 'react';
import PropTypes from 'prop-types';
import { Pagination, Processing, Icon } from 'app/components/ui';
import { NavLink } from 'react-router-dom';

const Records = ({ list: { status, pagination, data }, pageSet, remove }) => (
  <div className="records">
    <Processing processing={status.processing} />
    <h1 className="is-size-1">Records</h1>
    <table className="table">
      <thead>
        <tr>
          <th className="is-unselectable">Title</th>
          <th className="is-unselectable">Anons</th>
          <th className="is-unselectable">Description</th>
          <th className="is-unselectable">Actions</th>
        </tr>
      </thead>
      <tbody>
        {data.map(({ id, title, anons, description }) => (
          <tr key={id}>
            <td>{title}</td>
            <td>{anons}</td>
            <td>{description}</td>
            <td>
              <NavLink to={`/admin/blog/records/${id}`}>
                <Icon type="pencil-alt" color="dark" />
              </NavLink>

              <a onClick={remove.bind(null, id)}>
                <Icon type="trash" color="danger" />
              </a>
            </td>
          </tr>
        ))}
      </tbody>
      <tfoot>
        <tr>
          <td colSpan="6">
            <Pagination pagination={pagination} pageSet={pageSet} />
          </td>
        </tr>
      </tfoot>
    </table>
  </div>
);

Records.propTypes = {
  list: PropTypes.object,
  pageSet: PropTypes.func,
  remove: PropTypes.func,
};

export default Records;
