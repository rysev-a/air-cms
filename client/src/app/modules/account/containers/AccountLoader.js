import { connect } from 'react-redux';
import AccountLoader from '../components/AccountLoader';

const mapStateToProps = ({ account }) => ({ account });

export default connect(mapStateToProps)(AccountLoader);
