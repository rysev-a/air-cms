import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import AccountMenu from '../components/AccountMenu';
import accountActions from '../actions';
import { lifecycle, compose } from 'recompose';

const mapStateToProps = ({ account, router }) => ({ account, router });

const mapDispatchToProps = dispatch =>
  bindActionCreators(accountActions, dispatch);

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  lifecycle({
    componentDidMount() {
      this.props.redirect(this.props);
    },
    componentDidUpdate() {
      this.props.redirect(this.props);
    },
  })
)(AccountMenu);
