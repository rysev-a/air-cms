import React from 'react';
import PropTypes from 'prop-types';
import { SignMenu } from 'app/modules/sign';
import { NavLink } from 'react-router-dom';

const AccountMenu = ({ account }) => (
  <div className="navbar-menu">
    {account.isAuth && (
      <NavLink className="navbar-item" to="/admin/info">
        Info
      </NavLink>
    )}
    <div className="navbar-item has-dropdown is-hoverable">
      <a className="navbar-link">
        {account.loaded && account.isAuth ? account.data.email : 'Profile'}
      </a>
      <div className="navbar-dropdown">
        {account.isAuth && (
          <NavLink className="navbar-item" to={`/users/${account.data.id}`}>
            Settings
          </NavLink>
        )}
        <SignMenu />
      </div>
    </div>
  </div>
);

AccountMenu.propTypes = {
  account: PropTypes.object,
};

export default AccountMenu;
