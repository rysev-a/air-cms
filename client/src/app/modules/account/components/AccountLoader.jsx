import React from 'react';
import PropTypes from 'prop-types';
import { Processing } from 'app/components/ui';

const AccountLoader = ({ account, children }) => (
  <div className="account-loader">
    <Processing processing={!account.loaded} />
    {account.loaded && children}
  </div>
);

AccountLoader.propTypes = {
  account: PropTypes.object,
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

export default AccountLoader;
