import constants from './constants';

export const defaultState = () => ({
  data: {},
  loaded: false,
  isAuth: false,
  processing: false,
});

const accountReducer = (state = defaultState(), action) => {
  switch (action.type) {
    case constants.ACCOUNT_FETCH_SUCCESS:
      return {
        ...state,
        data: action.payload,
        processing: false,
        loaded: true,
        isAuth: true,
      };

    case constants.ACCOUNT_FETCH_ERROR:
      return {
        ...defaultState(),
        loaded: true,
      };

    case constants.ACCOUNT_FETCH_START:
      return {
        ...state,
        processing: true,
      };

    case constants.ACCOUNT_RESET:
      return defaultState();

    default:
      return state;
  }
};

export default accountReducer;
