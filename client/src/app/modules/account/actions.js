import constants from './constants';
import { accountApi } from 'app/services/api';
import { push } from 'react-router-redux';

const accountActions = {
  load: () => dispatch => {
    dispatch(accountActions.fetchStart());

    return accountApi
      .load()
      .then(({ data }) => dispatch(accountActions.fetchSuccess(data)))
      .catch(() => dispatch(accountActions.fetchError()));
  },

  fetchStart: () => ({
    type: constants.ACCOUNT_FETCH_START,
  }),

  fetchSuccess: data => ({
    type: constants.ACCOUNT_FETCH_SUCCESS,
    payload: data,
  }),

  fetchError: () => ({
    type: constants.ACCOUNT_FETCH_ERROR,
  }),

  reset: () => ({
    type: constants.ACCOUNT_RESET,
  }),

  redirect: ({ account, router }) => dispatch => {
    if (
      !account.isAuth &&
      account.loaded &&
      router.location.pathname != '/admin/signin'
    ) {
      dispatch(push('/admin/signin'));
    }
  },
};

export default accountActions;
