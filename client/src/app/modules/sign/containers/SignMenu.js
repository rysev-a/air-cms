import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SignMenu from '../components/SignMenu';
import signActions from '../actions';

const mapStateToProps = ({ account }) => ({ account });

const mapDispatchToProps = dispatch =>
  bindActionCreators(signActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignMenu);
