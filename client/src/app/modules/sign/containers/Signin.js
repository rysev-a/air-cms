import { withFormik } from 'formik';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Signin from '../components/Signin';
import signinActions from '../actions';

import { compose, lifecycle } from 'recompose';

const mapStateToProps = ({ account }) => ({ account });

const mapDispatchToProps = dispatch =>
  bindActionCreators(signinActions, dispatch);

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  lifecycle({
    componentDidMount() {
      this.props.redirect(this.props);
    },
    componentDidUpdate() {
      this.props.redirect(this.props);
    },
  }),
  withFormik({
    mapPropsToValues: () => ({ email: '', password: '' }),
    handleSubmit: (values, { props, setSubmitting, setErrors }) => {
      props.submit({ values, setSubmitting, setErrors });
    },
  })
)(Signin);
