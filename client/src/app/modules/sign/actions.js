import { accountApi } from 'app/services/api';
import accountActions from 'app/modules/account/actions';
import { push } from 'react-router-redux';

const signinActions = {
  submit: ({ values, setSubmitting, setErrors }) => dispatch =>
    accountApi
      .signin(values)
      .then(() => dispatch(accountActions.load()))
      .catch(({ response: { data } }) => {
        setSubmitting(false);
        setErrors(data.message);
      }),

  signout: () => dispatch =>
    accountApi.signout().then(() => dispatch(accountActions.load())),

  redirect: ({ account }) => dispatch => {
    if (account.isAuth) {
      dispatch(push('/admin'));
    }
  },
};

export default signinActions;
