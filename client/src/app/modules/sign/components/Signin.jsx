import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Button, Processing } from 'app/components/ui';
import './Signin.css';

const Signin = ({
  values,
  errors,
  handleChange,
  handleBlur,
  handleSubmit,
  isSubmitting,
}) => (
  <div className="level">
    <div className="level-item">
      <section className="section">
        <h2 className="title is-2">Signin Form</h2>
        <form className="signin-form" onSubmit={handleSubmit}>
          <Processing processing={isSubmitting} />
          <div className="field">
            <label className="label">Email</label>
            <div className="control">
              <input
                className={classNames('input', {
                  'is-danger': errors['email'],
                })}
                type="email"
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />
              {errors['email'] && (
                <p className="help is-danger">{errors['email']}</p>
              )}
            </div>
          </div>

          <div className="field">
            <label className="label">Password</label>
            <div className="control">
              <input
                className={classNames('input', {
                  'is-danger': errors['password'],
                })}
                type="password"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
              {errors['password'] && (
                <p className="help is-danger">{errors['password']}</p>
              )}
            </div>
          </div>
          <Button type="submit">Submit</Button>
        </form>
      </section>
    </div>
  </div>
);

Signin.propTypes = {
  values: PropTypes.object,
  errors: PropTypes.object,
  touched: PropTypes.object,
  handleChange: PropTypes.func,
  handleBlur: PropTypes.func,
  handleSubmit: PropTypes.func,
  isSubmitting: PropTypes.bool,
};

export default Signin;
