import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

const SignMenu = ({ account, signout }) =>
  account.loaded ? (
    <div className="sign-menu">
      {account.isAuth ? (
        <a className="navbar-item" onClick={signout}>
          Sign Out
        </a>
      ) : (
        <NavLink className="navbar-item" to="/signin">
          Signin
        </NavLink>
      )}
    </div>
  ) : null;

SignMenu.propTypes = {
  signout: PropTypes.func,
  account: PropTypes.object,
};

export default SignMenu;
