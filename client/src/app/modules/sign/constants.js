export default {
  SIGNIN_START: 'signin start',
  SIGNIN_SUCCESS: 'signin success',
  SIGNIN_ERROR: 'signin error',
};
