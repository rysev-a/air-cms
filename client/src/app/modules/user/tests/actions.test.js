import thunk from 'redux-thunk';
import moxios from 'moxios';
import configureMockStore from 'redux-mock-store';

import constants from '../constants';
import { defaultState } from '../reducers';
import { API_URL } from 'app/settings';
import userActions from '../actions';

const mockStore = configureMockStore([thunk]);

describe('user actions', () => {
  beforeEach(() => moxios.install());
  afterEach(() => moxios.uninstall());

  it('load user', () => {
    const id = 5;
    const userPayload = {
      id,
      email: 'testmail@mail.com',
    };

    moxios.stubRequest(`${API_URL}/users/${id}`, {
      status: 200,
      response: userPayload,
    });

    const expectedActions = [
      { type: constants.USER_ITEM_FETCH_START },
      { type: constants.USER_ITEM_FETCH_SUCCESS, payload: userPayload },
    ];

    const store = mockStore({ user: defaultState() });

    store
      .dispatch(userActions.load(id))
      .then(() => expect(store.getActions()).toEqual(expectedActions));
  });

  it('update user', () => {
    const id = 5;
    const userPayload = {
      id,
      email: 'testmail@mail.com',
    };

    moxios.stubRequest(`${API_URL}/users/${id}`, {
      status: 200,
      response: userPayload,
    });

    const expectedActions = [
      { type: constants.USER_ITEM_UPDATE_START },
      { type: constants.USER_ITEM_UPDATE_SUCCESS, payload: userPayload },
    ];

    const store = mockStore({ user: defaultState() });

    store
      .dispatch(userActions.update(id))
      .then(() => expect(store.getActions()).toEqual(expectedActions));
  });

  it('reset user', () => {
    const expectedAction = { type: constants.USER_ITEM_RESET };

    expect(userActions.reset()).toEqual(expectedAction);
  });
});
