import constants from './constants';

export const defaultState = () => ({
  data: {
    email: '',
    first_name: '',
    last_name: '',
    role: {
      name: '',
      id: 0,
    },
  },
  loaded: false,
  processing: false,
});

const userReducer = (state = defaultState(), action) => {
  switch (action.type) {
    case constants.USER_ITEM_FETCH_SUCCESS:
      return {
        ...state,
        data: action.payload,
        processing: false,
        loaded: true,
      };

    case constants.USER_ITEM_FETCH_START:
      return {
        ...state,
        processing: true,
      };

    case constants.USER_ITEM_RESET:
      return defaultState();

    default:
      return state;
  }
};

export default userReducer;
