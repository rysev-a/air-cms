import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import User from '../components/User';
import userActions from '../actions';
import { withRouter } from 'react-router';
import { compose, lifecycle } from 'recompose';
import { withFormik } from 'formik';

const mapStateToProps = ({ user, roles }) => ({ user, roles });

const mapDispatchToProps = dispatch =>
  bindActionCreators(userActions, dispatch);

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params;
      this.props.load(id);
    },
    componentWillUnmount() {
      this.props.reset();
    },
  }),

  withFormik({
    mapPropsToValues: ({ user }) => ({
      email: user.data.email,
      first_name: user.data.first_name,
      last_name: user.data.last_name,
      role_id: user.data.role.id,
    }),
    validate: (values, props) => {},
    handleSubmit: (
      values,
      { props: { user, update }, setSubmitting, setErrors }
    ) => {
      update({ id: user.data.id, values });
    },
    enableReinitialize: true,
  })
)(User);
