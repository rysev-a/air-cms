import constants from './constants';
import { userApi } from 'app/services/api';
import { push } from 'react-router-redux';

const userActions = {
  load: id => dispatch => {
    dispatch(userActions.fetchStart());

    return userApi
      .get(id)
      .then(({ data }) => dispatch(userActions.fetchSuccess(data)));
  },

  fetchStart: () => ({
    type: constants.USER_ITEM_FETCH_START,
  }),

  fetchSuccess: data => ({
    type: constants.USER_ITEM_FETCH_SUCCESS,
    payload: data,
  }),

  reset: () => ({
    type: constants.USER_ITEM_RESET,
  }),

  update: ({ id, values }) => dispatch => {
    return userApi.put({ id, values }).then(() => {
      dispatch(push('/admin/users'));
    });
  },
};

export default userActions;
