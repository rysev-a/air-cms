export default {
  USER_ITEM_FETCH_START: 'user item fetch start',
  USER_ITEM_FETCH_SUCCESS: 'user item fetch success',
  USER_ITEM_FETCH_ERROR: 'user item fetch error',

  USER_ITEM_UPDATE_START: 'user item update start',
  USER_ITEM_UPDATE_SUCCESS: 'user item update success',
  USER_ITEM_UPDATE_ERROR: 'user item update error',

  USER_ITEM_RESET: 'user item reset',
};
