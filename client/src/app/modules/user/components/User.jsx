import React from 'react';
import PropTypes from 'prop-types';
import { Processing, Button } from 'app/components/ui';
import { map } from 'ramda';
import './User.css';

const User = ({ values, handleChange, handleBlur, handleSubmit, roles }) => (
  <div className="user">
    <form className="user-form" onSubmit={handleSubmit}>
      <div className="field">
        <label className="label">Email:</label>
        <div className="control">
          <input
            name="email"
            className="input"
            value={values.email}
            onChange={handleChange}
            onBlur={handleBlur}
          />
        </div>
      </div>

      <div className="field">
        <label className="label">First Name:</label>
        <div className="control">
          <input
            name="first_name"
            className="input"
            value={values.first_name}
            onChange={handleChange}
            onBlur={handleBlur}
          />
        </div>
      </div>

      <div className="field">
        <label className="label">Last Name:</label>
        <div className="control">
          <input
            name="last_name"
            className="input"
            value={values.last_name}
            onChange={handleChange}
            onBlur={handleBlur}
          />
        </div>
      </div>

      <div className="field">
        <label className="label">Role:</label>
        <div className="control">
          <div className="select">
            <select
              name="role_id"
              value={values.role_id}
              onChange={handleChange}
              onBlur={handleBlur}>
              {map(
                role => (
                  <option key={role.id} value={role.id}>
                    {role.name}
                  </option>
                ),
                roles.data
              )}
            </select>
          </div>
        </div>
      </div>

      <Button type="submit">Submit</Button>
    </form>
  </div>
);

User.propTypes = {
  user: PropTypes.object,
  roles: PropTypes.object,
  values: PropTypes.object,
  handleBlur: PropTypes.func,
  handleChange: PropTypes.func,
  handleSubmit: PropTypes.func,
};

export default User;
