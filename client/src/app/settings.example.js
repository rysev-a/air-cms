const API_URL = '/api/v1';
const REDUX_LOGGER = false;

export { API_URL, REDUX_LOGGER };
