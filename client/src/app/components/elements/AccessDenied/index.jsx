import React from 'react';
import PropTypes from 'prop-types';

const AccessDenied = ({
  info: {
    response: { data, config },
  },
}) => (
  <div className="card">
    <header className="card-header">
      <p className="card-header-title">Access Denied</p>
    </header>
    <div className="card-content">
      <div className="content">
        {data.message && (
          <div className="access-detail">
            <p>
              URL: <a href={config.url}>{config.url}</a>
            </p>
            <p>Method: {config.method}</p>
            <p>Message: {data.message}</p>
          </div>
        )}
      </div>
    </div>
  </div>
);

AccessDenied.propTypes = {
  info: PropTypes.object,
};

export default AccessDenied;
