import React from 'react';
import { NavLink } from 'react-router-dom';
import { AccountMenu } from 'app/modules/account';
import { Icon } from 'app/components/ui';
import './index.css';

const Header = () => (
  <header className="header">
    <div className="container">
      <nav className="navbar" role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
          <NavLink className="navbar-item" to="/admin">
            <Icon type="cloud" size="large" />
          </NavLink>
          <a
            role="button"
            className="navbar-burger"
            aria-label="menu"
            aria-expanded="false">
            <span aria-hidden="true" />
            <span aria-hidden="true" />
            <span aria-hidden="true" />
          </a>
        </div>

        <div className="navbar-end">
          <AccountMenu />
        </div>
      </nav>
    </div>
  </header>
);

export default Header;
