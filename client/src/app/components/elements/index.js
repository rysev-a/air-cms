import Header from './Header';
import AsideMenu from './AsideMenu';
import Footer from './Footer';
import AccessDenied from './AccessDenied';
import Info from './Info';

export { Header, AsideMenu, Footer, AccessDenied, Info };
