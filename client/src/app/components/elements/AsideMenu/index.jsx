import React from 'react';
import { NavLink } from 'react-router-dom';

const AsideMenu = () => (
  <aside className="menu">
    <p className="menu-label">Administration</p>
    <ul className="menu-list">
      <li>
        <NavLink exact activeClassName="is-active" to="/admin/users">
          Users
        </NavLink>
      </li>
    </ul>
    <p className="menu-label">Blog</p>
    <ul className="menu-list">
      <li>
        <NavLink activeClassName="is-active" to="/admin/blog/records">
          My records
        </NavLink>
      </li>
      <li>
        <NavLink activeClassName="is-active" to="/admin/blog/create-record">
          Add record
        </NavLink>
      </li>
    </ul>
  </aside>
);

export default AsideMenu;
