import React from 'react';

const Footer = () => (
  <footer className="footer">
    <div className="content has-text-centered">
      <p>
        <strong>Air cms</strong> by{' '}
        <a target="__blank" href="https://github.com/rysev-a/">
          Alexey Rysev
        </a>. The source code is licensed
      </p>
    </div>
  </footer>
);

export default Footer;
