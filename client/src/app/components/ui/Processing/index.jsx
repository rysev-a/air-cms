import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './index.css';

const Processing = ({ processing }) => (
  <div
    className={classNames({
      processing: true,
      active: processing,
    })}>
    <div className="ball-pulse">
      <div />
      <div />
      <div />
    </div>
  </div>
);

Processing.propTypes = {
  processing: PropTypes.bool,
};

export default Processing;
