import * as R from 'ramda';

const addSeparators = R.reduce((arr, item) => {
  const prev = arr[arr.length - 1];
  if (!prev || item - prev == 1) {
    return [...arr, item];
  }

  const result = [...arr, '...', item];
  return result;
}, []);

const calculate = ({ page, pageCount }) => {
  const pageRangeDisplayed = 3;
  const marginPagesDisplayed = 1;

  const pageArray = R.compose(
    addSeparators,
    R.filter(item => item > 0 && item <= pageCount),
    R.uniq
  )([
    1,
    ...R.range(2, 2 + marginPagesDisplayed),
    ...R.range(page - pageRangeDisplayed, page + pageRangeDisplayed + 1),
    ...R.range(pageCount - marginPagesDisplayed, pageCount + 1),
  ]);

  return pageArray;
};

export default calculate;
