import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// import only if need icon
import {
  faPencilAlt,
  faTrash,
  faCloud,
} from '@fortawesome/free-solid-svg-icons';

library.add(faPencilAlt, faTrash, faCloud);

const Icon = ({ type, color, size }) => (
  <span
    className={classNames('icon', {
      [`is-${size}`]: size,
    })}>
    <FontAwesomeIcon
      icon={type}
      className={classNames({
        [`has-text-${color}`]: color,
        [`fa-lg`]: size === 'large',
      })}
    />
  </span>
);

Icon.propTypes = {
  type: PropTypes.string,
  color: PropTypes.string,
  size: PropTypes.string,
};

export default Icon;
