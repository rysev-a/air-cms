import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ children, type }) => (
  <button type={type} className="button">
    {children}
  </button>
);

Button.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
    PropTypes.string,
  ]),
  type: PropTypes.string,
};

export default Button;
