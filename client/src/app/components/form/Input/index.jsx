import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Input = props => (
  <div className={classNames('field', props.modificators)}>
    <div className="control">
      <input {...props} className="input" type="text" />
    </div>
  </div>
);

Input.propTypes = {
  props: PropTypes.object,
  modificators: PropTypes.array,
};

export default Input;
