import constants from '../constants';

const defaultState = () => [];

const filterReducer = (state = defaultState(), action) => {
  switch (action.type) {
    case constants.UPDATE_FILTER: {
      const { key, value } = action.payload;
      const updatedState = state.filter(item => item.key != key);

      if (!value) {
        return updatedState;
      }

      // if value, add filter
      return [...updatedState, { key, value }];
    }

    case constants.RESET:
      return defaultState();

    default:
      return state;
  }
};

export default filterReducer;
