import constants from '../constants';

const defaultState = () => [];

const dataReducer = (state = defaultState(), action) => {
  switch (action.type) {
    case constants.FETCH_SUCCESS:
      return action.payload.data;

    case constants.RESET:
      return defaultState();

    default:
      return state;
  }
};

export default dataReducer;
