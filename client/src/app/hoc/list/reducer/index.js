import { combineReducers } from 'redux';
import data from './data';
import pagination from './pagination';
import status from './status';
import filter from './filter';

export default combineReducers({ data, pagination, status, filter });
