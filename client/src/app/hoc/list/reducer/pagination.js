import constants from '../constants';

const defaultState = () => ({
  page: 1,
  pageCount: 0,
});

const paginationReducer = (state = defaultState(), action) => {
  switch (action.type) {
    case constants.FETCH_SUCCESS:
      return {
        ...state,
        pageCount: action.payload.page_count,
        page: action.payload.page,
      };

    case constants.RESET:
      return defaultState();

    default:
      return state;
  }
};

export default paginationReducer;
