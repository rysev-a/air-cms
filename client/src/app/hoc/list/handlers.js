import update from 'immutability-helper';
import actions from './actions';

const createHandlers = api => {
  const handlers = {
    load: ({ localDispatch, list }) => () => {
      localDispatch(actions.fetchStart());
      return api
        .get(list)
        .then(({ data }) => localDispatch(actions.fetchSuccess(data)));
    },

    pageSet: ({ localDispatch, list }) => page =>
      handlers.load({
        localDispatch,
        list: update(list, { pagination: { page: { $set: page } } }),
      })(),

    remove: ({ localDispatch, list }) => id => {
      localDispatch(actions.removeStart());
      api
        .delete(id)
        .then(handlers.load({ localDispatch, list }))
        .catch(() => localDispatch(actions.removeError()));
    },
  };

  return handlers;
};

export default createHandlers;
