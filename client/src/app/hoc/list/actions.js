import constants from './constants';

const actions = {
  fetchSuccess: payload => ({
    type: constants.FETCH_SUCCESS,
    payload,
  }),

  fetchStart: () => ({
    type: constants.FETCH_START,
  }),

  fetchError: () => ({
    type: constants.FETCH_SUCCESS,
  }),

  removeStart: () => ({
    type: constants.REMOVE_START,
  }),

  removeSuccess: () => ({
    type: constants.REMOVE_SUCCESS,
  }),

  removeError: () => ({
    type: constants.REMOVE_ERROR,
  }),

  updatePagination: payload => ({
    type: constants.UPDATE_PAGINATION,
    payload,
  }),

  updateFilter: payload => ({
    type: constants.UPDATE_FILTER,
    payload,
  }),
};

export default actions;
