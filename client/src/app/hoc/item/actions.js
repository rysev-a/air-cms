import constants from './constants';

const itemActions = {
  fetchStart: payload => ({
    type: constants.FETCH_START,
    payload,
  }),

  fetchSuccess: payload => ({
    type: constants.FETCH_SUCCESS,
    payload,
  }),

  fetchError: payload => ({
    type: constants.FETCH_ERROR,
    payload,
  }),

  updateStart: payload => ({
    type: constants.UPDATE_START,
    payload,
  }),

  updateSuccess: payload => ({
    type: constants.UPDATE_SUCCESS,
    payload,
  }),

  updateError: payload => ({
    type: constants.UPDATE_ERROR,
    payload,
  }),
};

export default itemActions;
