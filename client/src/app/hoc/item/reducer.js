import constants from './constants';

export const defaultState = () => ({
  data: {},
  loaded: false,
  processing: false,
});

const itemReducer = (state = defaultState(), action) => {
  switch (action.type) {
    case constants.FETCH_SUCCESS:
      return {
        ...state,
        data: action.payload,
        processing: false,
        loaded: true,
      };

    case constants.FETCH_START:
      return {
        ...state,
        processing: true,
      };

    default:
      return state;
  }
};

export default itemReducer;
