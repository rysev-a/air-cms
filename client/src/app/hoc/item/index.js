import itemReducer from './reducer';
import createHandlers from './handlers';
import { compose, withReducer, withHandlers } from 'recompose';

const withItem = (itemApi, listApi) =>
  compose(
    withReducer('item', 'localDispatch', itemReducer),
    withHandlers(createHandlers(itemApi, listApi))
  );

export { itemReducer, createHandlers, withItem };
