import actions from './actions';
import { push } from 'react-router-redux';

const createHandlers = (itemApi, listApi) => {
  const handlers = {
    load: ({
      localDispatch,
      match: {
        params: { id },
      },
    }) => () => {
      localDispatch(actions.fetchStart());
      return itemApi
        .get(id)
        .then(({ data }) => localDispatch(actions.fetchSuccess(data)))
        .catch(() => localDispatch(actions.fetchError()));
    },

    update: ({
      localDispatch,
      dispatch,
      item: {
        data: { id },
      },
    }) => ({ values, setSubmitting, setErrors }) =>
      itemApi
        .put({ id, values })
        .then(() => dispatch(push('/admin/blog/records'))),

    create: ({ dispatch }) => ({ values, setSubmitting, setErrors }) => {
      listApi.post(values).then(() => dispatch(push('/admin/blog/records')));
    },
  };

  return handlers;
};

export default createHandlers;
