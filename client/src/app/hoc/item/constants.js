export default {
  FETCH_START: 'fetch start',
  FETCH_SUCCESS: 'fetch success',
  FETCH_ERROR: 'fetch error',

  UPDATE_START: 'update start',
  UPDATE_SUCCESS: 'update success',
  UPDATE_ERROR: 'update error',
};
