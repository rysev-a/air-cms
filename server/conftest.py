import pytest
from app import create_app
from app.core.database import db


@pytest.fixture
def app():
    application = create_app('app.settings.test')
    with application.app_context():
        db.create_all()
        yield application
        db.session.remove()
        db.drop_all()


@pytest.fixture
def client(app):
    with app.test_client() as c:
        yield c


# require all conftest plugins
pytest_plugins = [
    'app.users.tests.plugins',
    'app.account.tests.plugins',
    'app.roles.tests.plugins',
    'app.blog.tests.plugins',
]
