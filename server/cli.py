import os
import click
from app import create_app
from app.core.database import db

from app.users.models import User
from app.roles.models import Role
from app.users.commands import user_cli
from app.roles.commands import role_cli
from app.blog.commands import blog_cli

"""
How to:
1. `export FLASK_APP=cli.py`
2. `flask run`
"""
app = create_app(os.environ['SETTINGS_ENV'] or 'app.settings.development')

app.cli.add_command(role_cli)
app.cli.add_command(user_cli)
app.cli.add_command(blog_cli)


@app.shell_context_processor
def make_shell_context():
    return dict(app=app, db=db, User=User, Role=Role)
