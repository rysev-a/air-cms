from functools import wraps
from flask_login import current_user
from flask import make_response, jsonify, current_app


def access_required(roles):
    def wrapper(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            # disable permissions check in testing
            if current_app.config.get('LOGIN_DISABLED'):
                return f(*args, **kwargs)

            if current_user.role.name in roles:
                return f(*args, **kwargs)
            else:
                return make_response(jsonify({
                    'message': 'access denied'
                }), 403)
        return decorated_function
    return wrapper
