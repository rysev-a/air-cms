from flask import render_template
from app.blog.models import Record


def index_page():
    return render_template('pages/index.html')


def admin_page(*args, **kwargs):
    return render_template('pages/admin.html')


def blog_list():
    records = Record.query.all()
    data = {'records': records}
    return render_template('pages/blog-list.html', **data)


def blog_item(id):
    record = Record.query.get(id)
    return render_template('pages/blog-item.html', record=record)
