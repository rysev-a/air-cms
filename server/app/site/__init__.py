from flask import Blueprint
from .views import index_page, blog_list, blog_item, admin_page

site = Blueprint('site', __name__, template_folder='templates',
                 static_url_path='/static/site',
                 static_folder='static')


site.add_url_rule('/', 'index', index_page)
site.add_url_rule('/blog', 'blog_list', blog_list)
site.add_url_rule('/blog/<id>', 'blog_item', blog_item)

site.add_url_rule('/admin', 'admin_page', admin_page)
site.add_url_rule('/admin/<path:allurl>', 'admin_page', admin_page)
