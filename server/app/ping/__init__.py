from app.core.api import api
from .resources import Ping


api.add_resource(Ping, '/api/v1/ping')
