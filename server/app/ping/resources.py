from flask_restful import Resource


class Ping(Resource):
    @staticmethod
    def get():
        return {"pong": 1}
