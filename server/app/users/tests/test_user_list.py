def test_get_users(user_api, test_user):
    response = user_api.get_list()

    assert response.status_code == 200
    assert len(response.json.get('data')) == 1


def test_users_page_count(user_api, test_user_data, create_user):
    for i in range(1, 20):
        email = f'{i}-{test_user_data.get("email")}'
        create_user({**test_user_data, 'email': email})

    response = user_api.get_list()
    assert len(response.json.get('data')) == 10
    assert response.json.get('page_count') == 2


def test_users_page_value(user_api, test_user_data, create_user):
    for i in range(1, 16):
        email = f'{i}-{test_user_data.get("email")}'
        create_user({**test_user_data, 'email': email})

    respose = user_api.get_list(page=2)
    data = respose.json.get('data')
    assert len(data) == 5


def test_users_filter(user_api, test_user_data, create_user):
    for i in range(1, 16):
        email = f'{i}-{test_user_data.get("email")}'
        create_user({**test_user_data, 'email': email})

    respose = user_api.get_list(filter=[{'key': 'email', 'value': '11-'}])
    data = respose.json.get('data')

    assert len(data) == 1
