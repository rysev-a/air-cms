def test_get_user(user_api, test_user):
    id = test_user.id
    response = user_api.get_item(id)
    assert response.json.get('email') == test_user.email


def test_post_user(user_api, test_user_data):
    id = user_api.create_item(test_user_data).json.get('id')
    response = user_api.get_item(id)
    assert response.json.get('email') == test_user_data.get('email')


def test_delete_user(user_api, test_user):
    id = test_user.id
    response = user_api.delete_item(id)

    assert response.json.get('message') == 'success'
    assert response.json.get('id') == str(id)

    response = user_api.get_item(id)
    assert response.status == '404 NOT FOUND'


def test_update_user(user_api, test_user, test_user_data):
    id = test_user.id
    new_mail = 'newmail@mail.ru'
    new_data = {'email': new_mail}

    user_api.update_item(id, new_data)
    response = user_api.get_item(id)
    assert response.json.get('email') == new_mail
