import pytest
import json
from app.users.models import User
from app.core.database import db

TEST_DATA = {
    'email': 'test@email.com',
    'first_name': 'test_first_name',
    'last_name': 'test_last_name',
    'patronymic': 'test_patronymic',
    'phone': '8-111-111-1111',
    'password': 'test'
}

TEST_CLASS = User


class API():
    def __init__(self, client):
        self.client = client
        self.api_url = '/api/v1/users'

    def get_item(self, id):
        return self.client.get(f'{self.api_url}/{id}')

    def create_item(self, data):
        return self.client.post(self.api_url,
                                data=json.dumps(data),
                                content_type='application/json'
                                )

    def delete_item(self, id):
        return self.client.delete(f'{self.api_url}/{id}')

    def get_list(self, page=1, filter=None):
        filter = json.dumps(filter or {})
        url = f'{self.api_url}?page={page}&filter={filter}'
        return self.client.get(url)

    def update_item(self, id, data):
        return self.client.put(f'{self.api_url}/{id}',
                               data=json.dumps(data),
                               content_type='application/json')


@pytest.fixture()
def user_api(client):
    return API(client)


@pytest.fixture()
def test_user():
    item = TEST_CLASS(**TEST_DATA)
    db.session.add(item)
    db.session.commit()
    return item


@pytest.fixture()
def test_user_data():
    return TEST_DATA


@pytest.fixture()
def create_user():
    def func(data):
        item = TEST_CLASS(**data)
        db.session.add(item)
        db.session.commit()
    return func
