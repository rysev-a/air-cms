import json
import math
from flask_restful import marshal, request, reqparse, Resource
from app.core.database import db
from app.core.permissions import access_required
from app.roles.models import Role
from .models import User
from .fields import user_fields


class UserList(Resource):
    @access_required(['admin', 'developer'])
    def get(self):
        self.parser = reqparse.RequestParser()
        self.users = User.query

        # apply filters
        self.apply_filters()
        self.apply_pagination()

        return {
            'data': marshal(self.users.all(), user_fields),
            'page': self.page,
            'page_count': self.page_count
        }

    def apply_pagination(self):
        per_page = 10
        self.parser.add_argument('page', type=int)
        self.page = self.parser.parse_args().get('page') or 1
        self.page_count = math.ceil(self.users.count() / per_page)
        self.users = self.users.limit(per_page).offset(
            (self.page - 1) * per_page)

    def apply_filters(self):
        self.parser.add_argument('filter', type=str)
        self.filter = self.parser.parse_args().get('filter') or '[]'

        for filter_item in json.loads(self.filter):
            key = filter_item.get('key')
            value = filter_item.get('value')
            column = getattr(User, key, None)
            self.users = self.users.filter(column.like(f'%{value}%'))

    @staticmethod
    def post():
        user = User(**request.json)

        # set client role by default
        user.role = Role.query.filter_by(name='client').first()

        db.session.add(user)
        db.session.commit()
        return marshal(user, user_fields)


class UserItem(Resource):
    @staticmethod
    def get(id):
        user = User.query.get(id)
        if not user:
            return {'message': 'Not found'}, 404

        return marshal(user, user_fields)

    @staticmethod
    def delete(id):
        user = User.query.get(id)
        db.session.delete(user)
        db.session.commit()
        return {
            'message': 'success',
            'id': id
        }, 200

    @staticmethod
    def put(id):
        user_query = User.query.filter_by(id=id)
        user_query.update(request.json)
        db.session.commit()
        return marshal(user_query.first(), user_fields)
