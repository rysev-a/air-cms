from app.core.api import api
from .resources import UserItem, UserList

api.add_resource(UserList, '/api/v1/users')
api.add_resource(UserItem, '/api/v1/users/<id>')
