import click
import requests
from flask.cli import AppGroup
from app.core.database import db
from app.roles.models import Role
from .models import User


user_cli = AppGroup('user')


@user_cli.command('generate')
def generate():
    for item in range(0, 5):
        response = requests.get('https://randomuser.me/api/')
        for user_data in response.json().get('results'):
            user = {
                'email': user_data['email'],
                'first_name': user_data['name']['first'],
                'last_name': user_data['name']['last'],
                'patronymic': user_data['login']['username'],
                'phone': user_data['cell']
            }

            new_user = User(**user)
            new_user.role = Role.query.filter_by(name='client').first()
            db.session.add(new_user)
        db.session.commit()


@user_cli.command('clear')
def clear():
    User.query.delete()
    db.session.commit()


@user_cli.command('create')
@click.option('--email', prompt='User email')
@click.option('--password', prompt='User password')
@click.option('--role', prompt='User role',
              help='admin, developer, manager or client')
def create(email, password, role):
    role_id = Role.query.filter_by(name=role).first().id
    user = User(email=email, password=password, role_id=role_id)
    db.session.add(user)
    db.session.commit()
