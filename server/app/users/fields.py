from flask_restful import fields
from app.roles.fields import role_fields

user_fields = {
    'id': fields.String,
    'email': fields.String,
    'phone': fields.String,
    'first_name': fields.String,
    'last_name': fields.String,
    'patronymic': fields.String,
    'date_created': fields.DateTime,
    'date_updated': fields.DateTime,
    'is_active': fields.Boolean,
    'role': fields.Nested(role_fields)
}
