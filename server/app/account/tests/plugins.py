import json
import pytest

from app.core.database import db
from app.users.models import User


class API():
    def __init__(self, client):
        self.client = client
        self.api_url = '/api/v1/account'

    def signin(self, data):
        return self.client.post(f'{self.api_url}/signin',
                                data=json.dumps(data),
                                content_type='application/json'
                                )

    def signout(self):
        return self.client.post(f'{self.api_url}/signout')

    def info(self):
        return self.client.get(f'{self.api_url}')


@pytest.fixture()
def account_api(client):
    return API(client)


@pytest.fixture()
def test_account_data():
    return {
        'email': 'test_account@mail.fq',
        'password': 'test'
    }


@pytest.fixture()
def test_account(test_account_data):
    account = User(**test_account_data)
    db.session.add(account)
    db.session.commit()
    return account
