def test_signin(account_api, test_account_data, test_account):
    response = account_api.signin(test_account_data)

    assert response.status == '200 OK'
    assert response.json.get('message') == 'signin ok'


def test_info(account_api, test_account_data, test_account):
    account_api.signin(test_account_data)
    response = account_api.info()

    assert response.status == '200 OK'
    assert response.json.get('email') == test_account_data.get('email')


def test_signout(account_api, test_account_data, test_account):
    account_api.signin(test_account_data)
    account_api.signout()
    response = account_api.info()

    assert response.status == '400 BAD REQUEST'
    assert response.json.get('message') == 'not authincated'
