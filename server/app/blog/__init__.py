from app.core.api import api
from .resources import RecordItem, RecordList


api.add_resource(RecordItem, '/api/v1/blog/records/<int:id>')
api.add_resource(RecordList, '/api/v1/blog/records')
