from app.core.database import db
from sqlalchemy.sql import func


class Record(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(length=200), unique=True)
    description = db.Column(db.String(length=500))
    anons = db.Column(db.String(length=500))
    content = db.Column(db.String(length=15000))
    video = db.Column(db.String(length=200))
    created_at = db.Column(
        db.DateTime(timezone=True), default=func.now())
    updated_at = db.Column(
        db.DateTime(timezone=True),
        onupdate=func.now(),
        default=func.now()
    )
