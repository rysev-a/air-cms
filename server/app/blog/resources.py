import json
import math
from flask_restful import Resource, request, reqparse, marshal
from app.core.database import db
from app.core.permissions import access_required
from .fields import record_fields
from .models import Record


class RecordList(Resource):
    def get(self):
        self.parser = reqparse.RequestParser()
        self.records = Record.query

        # apply filters
        self.apply_filters()
        self.apply_sort()
        self.apply_pagination()

        return {
            'data': marshal(self.records.all(), record_fields),
            'page': self.page,
            'page_count': self.page_count
        }

    def apply_pagination(self):
        per_page = 10
        self.parser.add_argument('page', type=int)
        self.page = self.parser.parse_args().get('page') or 1
        self.page_count = math.ceil(self.records.count() / per_page)
        self.records = self.records.limit(per_page).offset(
            (self.page - 1) * per_page)

    def apply_filters(self):
        self.parser.add_argument('filter', type=str)
        self.filter = self.parser.parse_args().get('filter') or '[]'

        for filter_item in json.loads(self.filter):
            key = filter_item.get('key')
            value = filter_item.get('value')
            column = getattr(Record, key, None)
            self.records = self.records.filter(column.like(f'%{value}%'))

    def apply_sort(self):
        self.parser.add_argument('sort', type=str)
        sort = self.parser.parse_args().get('sort')

        if not sort:
            self.records = self.records.order_by(Record.updated_at.desc())

    @staticmethod
    def post():
        record = Record(**request.json)
        db.session.add(record)
        db.session.commit()
        return marshal(record, record_fields)


class RecordItem(Resource):
    @staticmethod
    def get(id):
        record = Record.query.get(id)
        if not record:
            return {'message': 'Not found'}, 404

        return marshal(record, record_fields)

    @staticmethod
    def delete(id):
        record = Record.query.get(id)
        db.session.delete(record)
        db.session.commit()
        return {
            'message': 'success',
            'id': id
        }, 200

    @staticmethod
    def put(id):
        record_query = Record.query.filter_by(id=id)
        record_query.update(request.json)
        db.session.commit()
        return marshal(record_query.first(), record_fields)
