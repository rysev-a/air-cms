from flask_restful import fields

record_fields = {
    'id': fields.Integer,
    'title': fields.String,
    'anons': fields.String,
    'description': fields.String,
    'content': fields.String,
    'video': fields.String,
    'created_at': fields.String,
    'updated_at': fields.String,
}
