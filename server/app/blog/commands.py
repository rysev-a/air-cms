import click
import requests
from flask.cli import AppGroup
from app.core.database import db
from .models import Record


blog_cli = AppGroup('blog')


@blog_cli.command('generate')
def generate():
    url = "https://my.api.mockaroo.com/blog/records.json?key=25d2cb90"
    response = requests.get(url)
    for item in response.json():
        db.session.add(Record(**item))

    db.session.commit()


@blog_cli.command('clear')
def clear():
    Record.query.delete()
    db.session.commit()
