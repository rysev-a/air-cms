def test_get_record(record_api, test_record):
    id = test_record.id
    response = record_api.get_item(id)
    assert response.json.get('title') == test_record.title


def test_post_record(record_api, test_record_data):
    id = record_api.create_item(test_record_data).json.get('id')
    response = record_api.get_item(id)
    assert response.json.get('title') == test_record_data.get('title')


def test_delete_record(record_api, test_record):
    id = test_record.id
    response = record_api.delete_item(id)

    assert response.json.get('message') == 'success'
    assert response.json.get('id') == id

    response = record_api.get_item(id)
    assert response.status == '404 NOT FOUND'


def test_update_record(record_api, test_record, test_record_data):
    id = test_record.id
    new_title = 'newmail@mail.ru'
    new_data = {'title': new_title}

    record_api.update_item(id, new_data)
    response = record_api.get_item(id)
    assert response.json.get('title') == new_title
