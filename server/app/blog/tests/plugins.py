import pytest
import json
from app.blog.models import Record
from app.core.database import db

TEST_DATA = {
    'title': 'test title',
    'anons': 'test anons',
    'description': 'test description',
    'content': 'test content'
}

TEST_CLASS = Record


class API():
    def __init__(self, client):
        self.client = client
        self.api_url = '/api/v1/blog/records'

    def get_item(self, id):
        return self.client.get(f'{self.api_url}/{id}')

    def create_item(self, data):
        return self.client.post(self.api_url,
                                data=json.dumps(data),
                                content_type='application/json'
                                )

    def delete_item(self, id):
        return self.client.delete(f'{self.api_url}/{id}')

    def get_list(self, page=1, filter=None):
        filter = json.dumps(filter or {})
        url = f'{self.api_url}?page={page}&filter={filter}'
        return self.client.get(url)

    def update_item(self, id, data):
        return self.client.put(f'{self.api_url}/{id}',
                               data=json.dumps(data),
                               content_type='application/json')


@pytest.fixture()
def record_api(client):
    return API(client)


@pytest.fixture()
def test_record():
    item = TEST_CLASS(**TEST_DATA)
    db.session.add(item)
    db.session.commit()
    return item


@pytest.fixture()
def test_record_data():
    return TEST_DATA


@pytest.fixture()
def create_record():
    def func(data):
        item = TEST_CLASS(**data)
        db.session.add(item)
        db.session.commit()
    return func
