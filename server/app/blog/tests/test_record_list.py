def test_get_records(record_api, test_record):
    response = record_api.get_list()

    assert response.status_code == 200
    assert len(response.json.get('data')) == 1


def test_records_page_count(record_api, test_record_data, create_record):
    for i in range(1, 20):
        title = f'{i}-{test_record_data.get("title")}'
        create_record({**test_record_data, 'title': title})

    response = record_api.get_list()
    assert len(response.json.get('data')) == 10
    assert response.json.get('page_count') == 2


def test_records_page_value(record_api, test_record_data, create_record):
    for i in range(1, 16):
        title = f'{i}-{test_record_data.get("title")}'
        create_record({**test_record_data, 'title': title})

    respose = record_api.get_list(page=2)
    data = respose.json.get('data')
    assert len(data) == 5
