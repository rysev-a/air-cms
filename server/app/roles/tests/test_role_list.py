def test_get_roles(role_api, test_role):
    response = role_api.get_list()
    assert response.status_code == 200
    assert len(response.json) == 1
