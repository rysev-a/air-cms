import pytest
import json
from app.roles.models import Role
from app.core.database import db


TEST_DATA = {
    'name': 'admin'
}

TEST_CLASS = Role


class API():
    def __init__(self, client):
        self.client = client
        self.api_url = '/api/v1/roles'

    def get_item(self, id):
        return self.client.get(f'{self.api_url}/{id}')

    def post_item(self, data):
        print('post item in confest')
        print(data)
        return self.client.post(self.api_url,
                                data=json.dumps(data),
                                content_type='application/json'
                                )

    def delete_item(self, id):
        return self.client.delete(f'{self.api_url}/{id}')

    def get_list(self):
        return self.client.get(self.api_url)


@pytest.fixture()
def role_api(client):
    return API(client)


@pytest.fixture()
def test_role():
    item = TEST_CLASS(**TEST_DATA)
    db.session.add(item)
    db.session.commit()
    return item


@pytest.fixture()
def test_role_data():
    return TEST_DATA
