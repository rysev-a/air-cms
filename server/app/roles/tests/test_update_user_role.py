from app.core.database import db


def test_update_user_role(app, user_api, test_user, test_role):
    new_data = {'role_id': test_role.id}
    response = user_api.update_item(test_user.id, new_data)
    assert test_user.role == test_role
