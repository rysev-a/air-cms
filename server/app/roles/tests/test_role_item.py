def test_get_role(role_api, test_role):
    response = role_api.get_item(test_role.id)
    assert response.json.get('name') == test_role.name


def test_post_role(role_api, test_role_data):
    id = role_api.post_item(test_role_data).json.get('id')
    response = role_api.get_item(id)
    assert response.json.get('name') == test_role_data.get('name')


def test_delete_role(role_api, test_role):
    id = test_role.id
    response = role_api.delete_item(id)

    assert response.json.get('message') == 'success'
    assert response.json.get('id') == id

    response = role_api.get_item(id)
    assert response.status == '404 NOT FOUND'
