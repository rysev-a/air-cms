from flask_restful import Resource, marshal, request
from app.core.database import db
from .models import Role
from .fields import role_fields


class RoleList(Resource):
    def get(self):
        roles = Role.query.all()
        return marshal(roles, role_fields)

    @staticmethod
    def post():
        data = request.json
        role = Role(**data)
        db.session.add(role)
        db.session.commit()
        return marshal(role, role_fields)


class RoleItem(Resource):
    @staticmethod
    def get(id):
        role = Role.query.get(id)
        if not role:
            return {'message': 'Not found'}, 404

        return marshal(role, role_fields)

    @staticmethod
    def delete(id):
        role = Role.query.get(id)
        db.session.delete(role)
        db.session.commit()
        return {
            'message': 'success',
            'id': id
        }, 200
