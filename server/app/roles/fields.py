from flask_restful import fields

role_fields = {
    'id': fields.Integer,
    'name': fields.String
}
