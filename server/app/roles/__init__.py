from app.core.api import api
from .resources import RoleItem, RoleList


api.add_resource(RoleItem, '/api/v1/roles/<int:id>')
api.add_resource(RoleList, '/api/v1/roles')
