import click
from flask.cli import AppGroup
from app.core.database import db
from .models import Role

role_cli = AppGroup('role')


@role_cli.command('generate')
def generate():
    for role_name in ['admin', 'developer', 'manager', 'client']:
        db.session.add(Role(name=role_name))

    db.session.commit()


@role_cli.command('clear')
def clear():
    Role.query.delete()
    db.session.commit()
