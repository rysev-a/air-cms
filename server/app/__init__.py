from flask import Flask

from app.core.api import api
from app.core.authorization import login_manager
from app.core.database import db
from app.core.migrate import migrate


# init site blueprint
from app.site import site

# init modules
from app import users
from app import account
from app import roles
from app import blog
from app import ping


def create_app(settings):
    app = Flask(__name__)
    app.config.from_object(settings)

    # init extensions
    db.init_app(app)
    migrate.init_app(app, db)
    login_manager.init_app(app)
    api.init_app(app)

    # init site blueprint
    app.register_blueprint(site)

    return app
